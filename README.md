# Lenside X API

The Lenside API with microservices. For more information, see the documentation in the `docs` folder.

## Services

- User Service
- Auth Service
- Post/Collection Service
- Interaction Service
- Recommendation Service
