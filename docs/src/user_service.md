<!--
✅❌
-->

# User Service
User service is responsible for managing user accounts and their rank. A user account is a collection of user information, such as user ID, username, avatar, etc. It's not responsible for authentication, which is handled by the auth service.

## RESTful API
RESTful API for user service except creating a new user account. Creating a new user account is handled gRPC as internal communication between services.

The RESTful API is exposed on port 8080. The following endpoints are available:
### `GET /{username}`
> ❌ not implemented yet

Returns a user's information. if username is not provided, it returns the current user's information.

#### Authentication
No authentication required if username is provided. If username is not provided, JWT of logged in user is required.

#### Params
- `detailed` (optional): If true, returns more information about the user, such as bio, follower count, etc.
#### Response
```JSON
{
    "avatar": "//url/to/avatar",
    "username": "asdf",
    "rank": {
        "id": 5,
        "name": "pro"
    },
    // the following fields are only available if passing detailed=true
    "bio": "string",
    "sponsored_count": 0,
    "follower_count": 0,
    "following_count": 0,
}
```

### `PATCH /`

> ❌ not implemented yet

Updates current user's information.
#### Authentication
- JWT of Logged in user
#### Request
```JSON
{
    "bio": "string",
    "username": "string",
}
```

#### Response

##### 204 No Content
Update successful.

##### 400 Bad Request
```json
[
    {
        "error": "string",
        "message": "string"
    },
]
```
Error is one of the following:
- `invalid_username`: Username is invalid.
- `username_taken`: Username is already taken.
- `invalid_bio`: Bio is invalid.

### `GET /following`
> ❌ not implemented yet

Returns a list of users that the current user follows.

### params
- `cur` (optional): Cursor to paginate through the list. If not provided, the first page is returned.

#### Authentication
- JWT of Logged in user

#### Response
```JSON
{
    "data": [
        {
            "avatar": "//url/to/avatar",
            "username": "asdf",
            "rank": {
                "id": 5,
                "name": "pro"
            },
        }
    ],
    "next": "string, may be empty",
    "prev": "string, may be empty"
}
```

## gRPC API
gRPC API for user service is used for creating a new user account. It's not exposed to the public. It's used internally by other services.

### `CreateUser`

> ❌ not implemented yet

Creates a new user account.
```protobuf

message CreateUserRequest {
    string username = 1;
    string password = 2;
    string email = 3;
}

message CreateUserResponse {
    enum Status {
        OK = 0;
        INVALID_USERNAME = 1;
        USERNAME_TAKEN = 2;
        INVALID_EMAIL = 3;
        EMAIL_TAKEN = 4;
        INTERNAL_ERROR = 5;
    }
    Status status = 1;
}

service UserService {
    rpc CreateUser(CreateUserRequest) returns (CreateUserResponse);
}

```